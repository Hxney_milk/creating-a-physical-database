CREATE DATABASE AuctionCentral;

\c domain_related_name;

CREATE SCHEMA auction_management;


SET search_path TO domain_schema;


CREATE TABLE seller (
    seller_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    address VARCHAR(100) NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE
);

CREATE TABLE auction (
    auction_id SERIAL PRIMARY KEY,
    date DATE NOT NULL CHECK (date > '2000-01-01'),
    place VARCHAR(100) NOT NULL,
    time TIME NOT NULL,
    specifics TEXT NOT NULL,
    seller_id INT NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (seller_id) REFERENCES seller(seller_id)
);

CREATE TABLE buyer (
    buyer_id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    address VARCHAR(100) NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE
);

CREATE TABLE auction_item (
    auction_item_id SERIAL PRIMARY KEY,
    auction_id INT NOT NULL,
    lot_number INT NOT NULL,
    starting_price DECIMAL NOT NULL CHECK (starting_price >= 0),
    verbal_description TEXT NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (auction_id) REFERENCES auction(auction_id)
);

CREATE TABLE item_category (
    category_id SERIAL PRIMARY KEY,
    auction_item_id INT NOT NULL,
    category_name VARCHAR(50) NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (auction_item_id) REFERENCES auction_item(auction_item_id)
);

CREATE TABLE item (
    item_id SERIAL PRIMARY KEY,
    category_id INT NOT NULL,
    auction_item_id INT NOT NULL,
    auction_claimed BOOLEAN NOT NULL DEFAULT FALSE,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (category_id) REFERENCES item_category(category_id),
    FOREIGN KEY (auction_item_id) REFERENCES auction_item(auction_item_id)
);

CREATE TABLE auction_sale (
    auction_sale_id SERIAL PRIMARY KEY,
    buyer_id INT NOT NULL,
    seller_id INT NOT NULL,
    actual_price DECIMAL NOT NULL CHECK (actual_price >= 0),
    auction_item_id INT NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (buyer_id) REFERENCES buyer(buyer_id),
    FOREIGN KEY (seller_id) REFERENCES seller(seller_id),
    FOREIGN KEY (auction_item_id) REFERENCES auction_item(auction_item_id)
);

CREATE TABLE auction_item_specifics (
    item_specifics_id SERIAL PRIMARY KEY,
    auction_item_id INT NOT NULL,
    specifics TEXT NOT NULL,
    auction_id INT NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (auction_item_id) REFERENCES auction_item(auction_item_id),
    FOREIGN KEY (auction_id) REFERENCES auction(auction_id)
);

CREATE TABLE seller_item (
    seller_item_id SERIAL PRIMARY KEY,
    seller_id INT NOT NULL,
    item_id INT NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (seller_id) REFERENCES seller(seller_id),
    FOREIGN KEY (item_id) REFERENCES item(item_id)
);

CREATE TABLE buyer_item (
    buyer_item_id SERIAL PRIMARY KEY,
    buyer_id INT NOT NULL,
    item_id INT NOT NULL,
    record_ts TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (buyer_id) REFERENCES buyer(buyer_id),
    FOREIGN KEY (item_id) REFERENCES item(item_id)
);

ALTER TABLE seller ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE auction ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE buyer ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;

